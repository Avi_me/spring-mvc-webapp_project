<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale1.0">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<title>Home</title>
<style type="text/css">
body {
	margin-bottom: 500px;
	background-color: lightcyan;
}

.bg-danger {
	background: linear-gradient(45deg, teal, orange, purple);
}

.bg-secondary {
	background: linear-gradient(45deg, skyblue, violet, blue);
}

.add {
	background-color: cyan;
	color: maroon;
	padding: 8px;
	text-decoration: none;
}

h1 {
	color: orangered;
}

th {
	color: white;
	padding: 10px;
}

#log {
	margin-left: 1060px;
	text-decoration: none;
	background-color: purple;
	color: white;
	padding: 10px;
	margin-top: -30px;
}
</style>

</head>
<body>

	<div class="container mt-5">
		<marquee>
			<h1>Welcome</h1>
		</marquee>
		<center>
			<h4>
				<u>Your Orders</u>
			</h4>
		</center>
		<a href="${pageContext.request.contextPath}/login.jsp" id="log">Log Out</a> <br> <br> <br>
		<br>

		<table>
			<tr>
				<td><a type="button"
					href="${pageContext.request.contextPath}/addProduct.jsp" class="btn btn-success col-4 my-2 ">Add Product</a></td>

			</tr>
		</table>

		<br> <br>
		<table
			class="table table-bordered table-hover table-striped text-white text-center">
			<thead class="text-uppercase bg-danger">
			   <th><center>Product Id</center></th>
				<th><center>Product Name</center></th>
				<th><center>Brand Name</center></th>
				<th><center>Product Type</center></th>
				<th><center>Quantity</center></th>
			</thead>
			<c:forEach items="${list}" var="product">
				<tr>
					<td>${product.id}</td>
					<td>${product.productName}</td>
					<td>${product.brand}</td>
					<td>${product.pType}</td>
					<td>${product.quantity}</td>
					<td><a type="button"
						href="${pageContext.request.contextPath}/editemp/${product.id}"
						class="btn btn-success w-100 px-4">Edit</a></td>
					<td><a type="button"
						href="${pageContext.request.contextPath}/delete/${product.id}"
						class="btn btn-success w-100">delete</a></td>
				</tr>
			</c:forEach>

		</table>


	</div>

	<h3>${message}</h3>
</body>
</html>