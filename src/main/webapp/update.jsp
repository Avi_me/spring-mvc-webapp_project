<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<title>Update</title>
<style type="text/css">
body {
	background-color: lightblue;
}

form {
	margin-left: 480px;
	margin-top: 80px;
	background: linear-gradient(45deg, teal, orangered, purple);
	padding: 20px;
	width: 360px;
	height: 200px;
	border-radius: 15px;
}

label {
	color: white;
	font-size: 20px;
	padding: 12px;
}

input {
	padding: 5px;
	border-radius: 5px;
	background-color: lightcyan;
}
</style>
</head>
<body>
	<center>
		<h1 style="margin-top: 100px; color: orangered">Please Enter your
			details to Update your Order List.</h1>
	</center>
	<form action="${pageContext.request.contextPath}/update" method="post">
		<table>
			<tr>
				<td><input type="hidden" name="id" value="${p.getId()}"></td>
			</tr>
			<tr>
				<td><label>Product Name</label></td>
				<td><input type="text" name="productName"
					value="${p.getProductName()}" class="my-0" required="required"></td>
			</tr>

			<tr>
				<td><label>Brand Name</label></td>
				<td><input type="text" name="brand" value="${p.getBrand()}"
					class="my-0" required="required"></td>
			</tr>

			<tr>
				<td><label>Product Type</label></td>
				<td><input type="text" name="pType" value="${p.getpType()}"
					class="my-0" required="required"></td>
			</tr>

			<tr>
				<td><label>Quantity</label></td>
				<td><input type="tel" name="quantity"
					value="${p.getQuantity()}" class="my-0" required="required"></td>
			</tr>
			<tr>
				<td><input type="submit" value="Update"
					style="font-size: 18px; background-color: cyan; color: maroon; margin-top: 10px; margin-left: 50px;">
					
			</tr>

		</table>
	</form>
</body>
</html>