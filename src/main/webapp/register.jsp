<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale1.0">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<title>Sign Up</title>
<style type="text/css">
form {
	margin-left: 700px;
	width: 450px;
	margin-top: 60px;
	border-radius: 10px 0px 10px 0px;
	padding: 15px;
}

.bg-primary {
	background: linear-gradient(45deg, teal, gray, orangered);
}

h2 {
	color: white;
	margin-left: 100px;
}

}
h3 {
	color: white;
	margin-left: 850px;
}

label {
	padding: 10px;
	font-size: 14px;
}

button {
	padding: 8px;
	background: linear-gradient(45deg, orangered, red);
	font-size: 19px;
}

body {
	background: linear-gradient(180deg, orange, skyblue, maroon);
	margin-bottom: 200px;
}
</style>
</head>
<body>
	<div class="container mt-3">

		<form action="registerUser" method="post" class="bg-primary p-3">

			<div class="form-group mt-2">
				<label class="">Name</label> <input type="text" name="name"
					class="form-control">
			</div>



			<div class="form-group mt-2">
				<label>Email</label> <input type="text" name="email"
					class="form-control">
			</div>



			<div class="form-group mt-2">
				<label>Password</label> <input type="password" name="password"
					class="form-control">
			</div>


			<div class="form-group mt-2">
				<label>Contact No.</label> <input type="tel" name="contact"
					class="form-control">
			</div>


			<div class="form-group mt-2">
				<label>Address</label>
				<textarea class="form-control" rows="4" name="address"></textarea>
			</div>


			<div class="form-group mt-2">
				<label>Pin Code</label> <input type="text" name="pin"
					class="form-control">
			</div>


			<div class="mt-3">
				<button type="submit">Sign Up</button>
			</div>

		</form>
	</div>
</body>
</html>