<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome User</title>
<style type="text/css">
body {
	background: linear-gradient(180deg, brown, skyblue,orangered);
	margin-bottom: 20px;
}
h2
{
 color:white;
 font-size: 30px;
}
p
{
  color:white;
  font-size:20px;
  text-align:justify;
  margin:auto;
  margin-top:100px;
  padding:20px;
  
}
div
{
 background:linear-gradient(45deg,purple,teal,orange);
 width:1000px;
 margin-left:170px;
 
}
a
{
  background-color:orange;
  color:white;
  padding:15px;
  margin-left:600px;
  margin-top:200px;
  font-size: 20px;
  text-decoration: none;
}
#spin
	{
		width: 100px;
		height: 100px;
		background-color: cyan;
		border-radius: 100%;
		padding: 50px;
		border-right:15px solid blueviolet;
		border-left: 15px solid deeppink;
		border-top: 45px solid teal;
		border-bottom: 45px solid darkred;
		animation: spin 2s linear infinite normal;
		margin-left: 40%;
	}
	@keyframes spin
	{
		from{
			transform: rotateZ(0deg);
			background-color: orange;
		}
		25%
		{
			transform: rotateZ(90deg);
			background-color: purple;
		}
		50%
		{
			transform:rotateZ(180deg);
			background-color: limegreen;
		}
		75%
		{
			transform: rotateZ(270deg);
			background-color: maroon;
		}
		to {
              transform: rotateZ(360deg);
              background-color: salmon;
		}

</style>
</head>
<body>
<h2><marquee>Welcome To Ben's Grocery Store</marquee></h2><br>


<div id="spin"></div><br><br>
<a href="${pageContext.request.contextPath}/display" title="click here to shop ">Shop</a>

<div>
<p>Welcome To our family.
   Here,you will find what you need for your daily use with ensured product
   Quality and best Price in the market.
   We deliver best of quality products, most of them are multi-national brands
   with Superior Quality.
   Please be assured about the quality of the product
   we are here to care about you and your family with our healthy
   products.<br>
   So, be free to shop from us.
   Happy Shopping. 
</p>
</div><br><br>
 
		<center>
		<h4><span style="color:black" >Copyright Protected&copy; 2020</span><br>
		<span style="color:black">All rights reserved&reg; : Ben'S Grocery Store</span></h4>
        </center>

</body>
</body>
</html>