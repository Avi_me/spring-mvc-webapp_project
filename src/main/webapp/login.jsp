<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale1.0">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<title>login</title>
<style type="text/css">
p {
	font-size: 20px;
	color: brown;
	margin-left: 750px;
}

a {
	text-decoration: none;
}

body {
	background: linear-gradient(90deg, teal, cyan);
}

form {
	margin-top: 100px;
	margin-left: 750px;
}

input {
	padding: 12px;
	margin-top: 10px;
}

h2 {
	color: darkblue;
}

h3 {
	margin-top: 100px;
	margin-left: 750px;
	background: linear-gradient(45deg, darkblue, orangered, cyan);
	color: white;
	padding: 20px;
	border-radius: 15px;
}
</style>
</head>
<body>

	<div class="container mt-2">
		<h2>
			<marquee> Welcome To Ben's Shop</marquee>
		</h2>

		<h3>
			Hi there !<br> Login here to Shop with Us.
		</h3>
		<form action="login" method="post" class="form-group mt-2" >
			<div>
				<input type="email" name="email" class="form-control input"
					placeholder="Enter your mail">
			</div>
			<br>
			<div>
				<input type="password" name="password" class="form-control"
					placeholder="Enter Password">
			</div>
			<br>
			<div>
				<button class="btn btn-primary">Sign In</button>
			</div>
		</form>
		<br>
		<p>
			Not a Member? <a href="register.jsp">Click here</a> To Register
		</p>
		
		<h4>${message}</h4>
	</div>
</body>
</html>