package com.Grocery.App.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Grocery.App.DAO.AuthDAO;
import com.Grocery.App.DTO.Login;
import com.Grocery.App.Entity.Register;

@Service
public class AuthService {
	@Autowired
	private AuthDAO authDAO;

	public void saveRegisterDetails(Register register) {
		authDAO.saveRegisterDetails(register);
	}

	public Register getRegsiterUserDataByEmailAndPassword(Login login) 
	{
		return authDAO.getRegsiterUserDataByEmailAndPassword(login);
	}
	
	
}
