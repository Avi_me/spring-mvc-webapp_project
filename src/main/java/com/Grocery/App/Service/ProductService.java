package com.Grocery.App.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Grocery.App.DAO.ProductDAO;
import com.Grocery.App.Entity.ProductsDetails;


@Service
public class ProductService 
{
	@Autowired
	private ProductDAO productDAO;
	
  public void saveProductDetails(ProductsDetails productDetails)
  {
	  productDAO.saveProductData(productDetails);
	  
  }
  
  public List<ProductsDetails> getProductDetailsByUserId(String id)
  {
	 return productDAO.getProductDetailsByUserId(id);
  }
  
 
  public ProductsDetails getProductDetailsById(String id)
	{
		return productDAO.getProductDetailsById(id);
	}
  
  
  public void updateProductDetails(ProductsDetails productsDetails)
  {
	  productDAO.updateProductsDetails(productsDetails);
  }
  
  public void deleteProductDetails(String id)
  {
	  productDAO.deleteProductsDetails(id);
  }
  
}
