package com.Grocery.App.Utils;

import java.io.Serializable;
import java.util.Random;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

public class CustomGenerator implements IdentifierGenerator
{

	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		String s="Prd@";
		
		Random random = new Random();
		int num = random.nextInt((10000-1)+10);
		String res=s+num;
		return res;
	}

}
