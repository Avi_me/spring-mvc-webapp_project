package com.Grocery.App.DAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.Grocery.App.DTO.Login;
import com.Grocery.App.Entity.Register;
@Repository
public class AuthDAO 
{

	@Autowired
	private SessionFactory sessionfactory;
  public void saveRegisterDetails(Register register)
  {
	 Session session = sessionfactory.openSession();
	 Transaction transaction = session.beginTransaction();
	 try
	 {
	 session.save(register);
	 transaction.commit();
	 }catch(Exception e)
	 {
		transaction.rollback();
		System.out.println("Save Operation Failed");
	 }
	 finally {
		session.close();
	}
  }
  
  public Register getRegsiterUserDataByEmailAndPassword(Login login)
  {
	  Session session = sessionfactory.openSession();
	  String hql="from Register where email=:e and password=:p ";
	  Query query = session.createQuery(hql);
	  query.setParameter("e", login.getEmail());
	  query.setParameter("p", login.getPassword());
	  Register uniqueResult = (Register) query.uniqueResult();
	  return uniqueResult;
	  
  }
  
 
  
  
}
