package com.Grocery.App.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.Grocery.App.Entity.ProductsDetails;

@Repository
public class ProductDAO {
	@Autowired
	private SessionFactory sessionfactory;

	public void saveProductData(ProductsDetails productDetails) {
		Session session = sessionfactory.openSession();
		try {
			Transaction transaction = session.beginTransaction();
			session.save(productDetails);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public List<ProductsDetails> getProductDetailsByUserId(String id) {
		Session session = sessionfactory.openSession();
		String hql = "from ProductsDetails where register.id=:uId";
		Query query = session.createQuery(hql);
		query.setParameter("uId", id);
		List<ProductsDetails> list = query.list();
		return list;
	}

	public ProductsDetails getProductDetailsById(String id) {
		Session session = sessionfactory.openSession();

		ProductsDetails productsDetails = session.get(ProductsDetails.class, id);
		if (productsDetails != null) {
			session.close();
			return productsDetails;
		} else {
			  System.out.println("Invalid Id!!");
			return null;
		}
	}

	public void updateProductsDetails(ProductsDetails productDetails) {
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.update(productDetails);
			transaction.commit();
		} catch (Exception e) 
		{
			transaction.rollback();
		}
	}
	
	
	public void deleteProductsDetails(String id)
	{
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		try
		{
		String hql="delete from ProductsDetails where id=:pId";
		Query query = session.createQuery(hql);
		query.setParameter("pId", id);
		query.executeUpdate();
		transaction.commit();
		}catch(Exception e)
		{
			transaction.rollback();
		System.out.println("Deletion Terminated");
		}
		
		
	}

}
