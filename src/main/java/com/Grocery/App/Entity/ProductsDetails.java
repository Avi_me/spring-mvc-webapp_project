package com.Grocery.App.Entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "product_details")
public class ProductsDetails implements Serializable
{
	@Id
	@GenericGenerator(name = "app_auto",strategy = "com.Grocery.App.Utils.CustomGenerator")
	@GeneratedValue(generator = "app_auto")
	
  @Column(name ="ID")
  private String id;
	
	@Column(name ="Product_Name")
  private String productName;
	
	@Column(name = "Brand")
  private String brand;
	
	@Column(name = "Product_Type")
  private String pType;
	
	@Column(name = "Qunatity")
  private Long quantity;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_Id")
	private Register register;
	


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getpType() {
		return pType;
	}

	public void setpType(String pType) {
		this.pType = pType;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}


	public Register getRegister() {
		return register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}

	@Override
	public String toString() {
		return "ProductsDetails [id=" + id + ", productName=" + productName + ", brand=" + brand + ", pType=" + pType
				+ ", quantity=" + quantity + ", register=" + register + "]";
	}
	

}
