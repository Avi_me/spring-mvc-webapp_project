package com.Grocery.App.Entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "register_users")
public class Register implements Serializable
{
	@Id
	@GenericGenerator(name = "register_auto",strategy = "com.Grocery.App.Utils.CustomGenerator")
	@GeneratedValue(generator = "register_auto")
	@Column(name = "ID")
    private String id;
	
	@Column(name = "Name")
    private String name;
	
	@Column(name = "Email_ID")
    private String email;
	
	@Column(name = "Password")
	private Long password;
	
	@Column(name = "Contact_No")
    private Long contact;
	
	@Column(name = "Address")
    private String address;
	
	@Column(name = "Pin_Code")
    private Long pin;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPassword() {
		return password;
	}

	public void setPassword(Long password) {
		this.password = password;
	}

	public Long getContact() {
		return contact;
	}

	public void setContact(Long contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getPin() {
		return pin;
	}

	public void setPin(Long pin) {
		this.pin = pin;
	}

	@Override
	public String toString() {
		return "Register [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", contact="
				+ contact + ", address=" + address + ", pin=" + pin + "]";
	}
	
}
