package com.Grocery.App.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.Grocery.App.DTO.Login;
import com.Grocery.App.Entity.ProductsDetails;
import com.Grocery.App.Entity.Register;
import com.Grocery.App.Service.AuthService;
import com.Grocery.App.Service.ProductService;

@Controller
@RequestMapping(value = "/")
public class AuthController 
{

	@Autowired
	private AuthService authService;
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping(value="/registerUser")
	 public ModelAndView saveUserData(Register register)
	 {
		 authService.saveRegisterDetails(register);
		 return new ModelAndView("login.jsp","message","Registration Successful! Please Login.");
	 }
	
	@RequestMapping(value = "/login")
	private ModelAndView loginCheck(Login login,HttpServletRequest request)
	{
		Register register = authService.getRegsiterUserDataByEmailAndPassword(login);
		if(register!=null)
		{
			HttpSession session = request.getSession();
			session.setAttribute("register", register);
			
			return new ModelAndView("welcome.jsp");
		}
		return new ModelAndView("login.jsp","message","Invalid Crendentials!!");
	}
	

}
