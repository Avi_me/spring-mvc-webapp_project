package com.Grocery.App.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.Grocery.App.Entity.ProductsDetails;
import com.Grocery.App.Entity.Register;
import com.Grocery.App.Service.ProductService;

@Controller
@RequestMapping(value = "/")
public class ProductController 
{
	@Autowired
	private ProductService productSevice;
	
	@RequestMapping(value = "/display")
	public ModelAndView home(HttpServletRequest request)
	{
		Register register = (Register) request.getSession().getAttribute("register");
		System.out.println(register);
		List<ProductsDetails> list = productSevice.getProductDetailsByUserId(register.getId());
		list.forEach(a->{
			System.out.println(a);
		});
		return new ModelAndView("home.jsp","list",list);
		
		
	}
	
	@RequestMapping(value = "/userProductDetails")
  public ModelAndView saveProductDetails(ProductsDetails productDetails,HttpServletRequest request)
  {
		System.out.println(productDetails);
		Register register = (Register) request.getSession().getAttribute("register");
		productDetails.setRegister(register);
		System.out.println(register);
		productSevice.saveProductDetails(productDetails);
		List<ProductsDetails> list = productSevice.getProductDetailsByUserId(register.getId());
		list.forEach(a->{
			System.out.println(a);
		});
		return new ModelAndView("home.jsp","list",list);
		
  }
	@RequestMapping(value = "/editemp/{id}")
	public ModelAndView getUserDetails(@PathVariable String id)
	{
	  ProductsDetails productDetailsById = productSevice.getProductDetailsById(id);	
	  System.out.println(productDetailsById);
	  return new ModelAndView("update.jsp","p",productDetailsById);
	}
	
	@RequestMapping(value="/update")   
	public ModelAndView updateProductsDetails(ProductsDetails productsDetails,HttpServletRequest request)
	{
		Register register = (Register) request.getSession().getAttribute("register");
		if(register!=null)
		{
			productsDetails.setRegister(register);
			System.out.println(register);
			productSevice.updateProductDetails(productsDetails);
			System.out.println("Update Successful.");
			List<ProductsDetails> list = productSevice.getProductDetailsByUserId(register.getId());
			 return new ModelAndView("home.jsp","list",list);
		}
		return new ModelAndView("home.jsp");
	}
	
	
	@RequestMapping(value="/delete/{id}") 
	public ModelAndView deleteProductDetails(@PathVariable String id,HttpServletRequest request)
	{
		productSevice.deleteProductDetails(id);
		System.out.println("Data Associated with "+ id+" has been deleted successfully.");
		Register register = (Register) request.getSession().getAttribute("register");
		List<ProductsDetails> list = productSevice.getProductDetailsByUserId(register.getId());
		return new ModelAndView("home.jsp","list",list);
		
	}
	
	
}
